# Help

## Problems with the library

If you encounter any problems with this library please report it to [GitLab](https://gitlab.com/vsb2024xd/ssp-maly-projekt/-/issues)

## Suggestions

If you want to add any features please make a pull request to the [GitLab](https://gitlab.com/vsb2024xd/ssp-maly-projekt/)

## Licensing

This project is licensed under MIT so do whatever the fuck you want.
