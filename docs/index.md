# Simple Math Library

FOSS project aimed at providing simple math formulas implementations in [Rust](https://www.rust-lang.org/).

## Installation

Add this repository as a dependency in your `Cargo.toml`

```toml
[dependencies]
simple_math = { git = "https://gitlab.com/vsb2024xd/ssp-maly-projekt" }
```

## Usage

```Rust
use simple_math::circle::Circle;
use simple_math::traits::HasArea;

fn main() {
    let a = Circle { r: 5.6 };

    println!("Area of the circle is {}", a.area().unwrap());
}
```
