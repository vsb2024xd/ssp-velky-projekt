# Simple Math Documentation

## Traits

### HasArea

#### Functions

```Rust
fn area(&self) -> Result<f64, &'static str>;
```

### TwoDimensions

This traits implements `HasArea`

#### Functions

```Rust
fn perimetr(&self) -> Result<f64, &'static str>;
```

### ThreeDimensions

This traits implements `HasArea`

#### Functions

```Rust
fn volume(&self) -> Result<f64, &'static str>;
```

## Structs

### Circle

```Rust
pub struct Circle {
    pub r: f64,
}
```

### Cube

```Rust
pub struct Cube {
    pub a: f64,
}
```

### Cuboid

#### Rectangular

```Rust
pub struct RectangularCuboid {
    pub a: f64,
    pub b: f64,
    pub c: f64,
}
```

### Rectangle

```Rust
pub struct Rectangle {
    pub a: f64,
    pub b: f64,
}
```

### Square

```Rust
pub struct Square {
    pub a: f64,
}
```
