use crate::traits::{HasArea, TwoDimensions};

pub struct Square {
    pub a: f64,
}

impl HasArea for Square {
    fn area(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 {
            return Err("Invalid square");
        }

        Ok(self.a * self.a)
    }
}

impl TwoDimensions for Square {
    fn perimetr(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 {
            return Err("Invalid square");
        }

        Ok(4.0 * self.a)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_area() {
        let a = Square { a: 3.0 };

        assert_eq!(a.area(), Ok(9.0));
    }

    #[test]
    fn test_area_invalid() {
        let b = Square { a: -5.0 };

        assert_ne!(b.area(), Ok(-25.0));
    }

    #[test]
    fn test_perimetr() {
        let a = Square { a: 3.3 };

        assert_eq!(a.perimetr(), Ok(13.2));
    }

    #[test]
    fn test_perimetr_invalid() {
        let b = Square { a: -5.0 };

        assert_ne!(b.perimetr(), Ok(-20.0));
    }
}
