use crate::traits::{HasArea, TwoDimensions};

pub struct Rectangle {
    pub a: f64,
    pub b: f64,
}

impl HasArea for Rectangle {
    fn area(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 || self.b < 0.0 {
            return Err("Invalid rectangle");
        }

        Ok(self.a * self.b)
    }
}

impl TwoDimensions for Rectangle {
    fn perimetr(&self) -> Result<f64, &'static str> {
        if self.a <= 0.0 || self.b <= 0.0 {
            return Err("Invalid rectangle");
        }

        Ok(2.0 * (self.a + self.b))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_area() {
        let a = Rectangle { a: 3.0, b: 2.0 };
        let b = Rectangle { a: 5.0, b: 0.0 };

        assert_eq!(a.area(), Ok(6.0));
        assert_eq!(b.area(), Ok(0.0));
    }

    #[test]
    fn test_area_invalid() {
        let b = Rectangle { a: -5.0, b: 1.0 };

        assert_ne!(b.area(), Ok(-5.0));
    }

    #[test]
    fn test_perimetr() {
        let a = Rectangle { a: 3.3, b: 2.8 };

        assert_eq!(a.perimetr(), Ok(12.2));
    }

    #[test]
    fn test_perimetr_invalid() {
        let b = Rectangle { a: 5.0, b: 0.0 };

        assert_ne!(b.perimetr(), Ok(-10.0));
    }
}
