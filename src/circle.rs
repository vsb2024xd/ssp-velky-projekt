use crate::traits::{HasArea, TwoDimensions};
use std::f64::consts::PI;

pub struct Circle {
    pub r: f64,
}

impl HasArea for Circle {
    fn area(&self) -> Result<f64, &'static str> {
        if self.r < 0.0 {
            return Err("Invalid circle");
        }

        Ok(PI * self.r * self.r)
    }
}

impl TwoDimensions for Circle {
    fn perimetr(&self) -> Result<f64, &'static str> {
        if self.r < 0.0 {
            return Err("Invalid circle");
        }

        Ok(2.0 * PI * self.r)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_area() {
        let a = Circle { r: 4.8 };

        assert_eq!(a.area(), Ok(PI * a.r * a.r))
    }

    #[test]
    fn test_area_invalid() {
        let a = Circle { r: -4.8 };

        assert_ne!(a.area(), Ok(PI * a.r * a.r))
    }

    #[test]
    fn test_perimetr() {
        let a = Circle { r: 1.0 / PI };

        assert_eq!(a.perimetr(), Ok(2.0));
    }

    #[test]
    fn test_perimetr_invalid() {
        let a = Circle { r: -1.0 / PI };

        assert_ne!(a.perimetr(), Ok(-2.0));
    }
}
