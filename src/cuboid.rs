use crate::traits::{HasArea, ThreeDimensions};

pub struct RectangularCuboid {
    pub a: f64,
    pub b: f64,
    pub c: f64,
}

impl HasArea for RectangularCuboid {
    fn area(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 || self.b < 0.0 || self.c < 0.0 {
            return Err("Invalid rectangular cuboid");
        }

        Ok(2.0 * (self.a * self.b + self.a * self.c + self.b * self.c))
    }
}

impl ThreeDimensions for RectangularCuboid {
    fn volume(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 || self.b < 0.0 || self.c < 0.0 {
            return Err("Invalid rectangular cuboid");
        }

        Ok(self.a * self.b * self.c)
    }
}

#[cfg(test)]
mod rectangular_cuboid_tests {
    use super::*;

    #[test]
    fn test_area() {
        let a = RectangularCuboid {
            a: 2.0,
            b: 3.0,
            c: 1.0,
        };

        assert_eq!(a.area(), Ok(22.0));
    }

    #[test]
    fn test_area_invalid() {
        let a = RectangularCuboid {
            a: 2.0,
            b: 3.0,
            c: -1.0,
        };

        assert_ne!(a.area(), Ok(-1.0));
    }

    #[test]
    fn test_volume() {
        let a = RectangularCuboid {
            a: 2.0,
            b: 3.0,
            c: 1.0,
        };

        assert_eq!(a.volume(), Ok(6.0));
    }

    #[test]
    fn test_volume_invalid() {
        let a = RectangularCuboid {
            a: 2.0,
            b: 3.0,
            c: -1.0,
        };

        assert_ne!(a.volume(), Ok(-6.0));
    }
}
