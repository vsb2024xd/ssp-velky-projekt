use crate::traits::{HasArea, ThreeDimensions};

pub struct Cube {
    pub a: f64,
}

impl HasArea for Cube {
    fn area(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 {
            return Err("Invalid square");
        }

        Ok(6.0 * self.a.powf(2.0))
    }
}

impl ThreeDimensions for Cube {
    fn volume(&self) -> Result<f64, &'static str> {
        if self.a < 0.0 {
            return Err("Invalid square");
        }

        Ok(self.a.powf(3.0))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_area() {
        let a = Cube { a: 3.0 };

        assert_eq!(a.area(), Ok(54.0));
    }

    #[test]
    fn test_area_invalid() {
        let b = Cube { a: -5.0 };

        assert_ne!(b.area(), Ok(-150.0));
    }

    #[test]
    fn test_volume() {
        let a = Cube { a: 3.0 };

        assert_eq!(a.volume(), Ok(27.0));
    }

    #[test]
    fn test_volume_invalid() {
        let b = Cube { a: -5.0 };

        assert_ne!(b.volume(), Ok(-125.0));
    }
}
