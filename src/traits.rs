pub trait HasArea {
    fn area(&self) -> Result<f64, &'static str>;
}

pub trait TwoDimensions: HasArea {
    fn perimetr(&self) -> Result<f64, &'static str>;
}

pub trait ThreeDimensions: HasArea {
    fn volume(&self) -> Result<f64, &'static str>;
}
